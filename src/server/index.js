const express = require('express');
const bodyParser = require('body-parser');

require('dotenv').config();

// initiate express
const app = express();
// add parser middleware to express instance
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// import user routes
const userRoutes = require('../entity/employee/routes');

// add user routes
app.use('/employees', userRoutes)

// basic routes
const port = process.env.PORT;
app.get('/', (req, res) => {
    res.status(200).json({
        success: true,
        message: `Server is on ${process.env.NODE_ENV} environment`,
        data: {},
        errors: []
    });
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

module.exports = app;
