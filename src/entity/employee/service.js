const employeeModel = require('./model');

const getEmployeeDetails = async (id) => {
    let employee;

    const employeeList = await employeeModel.getAllEmployees();
    employeeList.map((member) => ( (member.employeeId == id) ? employee = member : ''))

    if (!employee){
        return employee;
    }

    const currencyList = await employeeModel.getAllCurrencyRates();
    currencyList.map((member) => {
        if (employee.branch == member.country){
            employee.type = member.type;
            employee.rate = member.rate;
        }
    })

    let returnEmployee = {};
    returnEmployee.employeeId = employee.employeeId;
    returnEmployee.fullName = employee.fullName;
    returnEmployee.branch = employee.branch;

    let salary = (employee.salary * employee.rate).toFixed(2)
    returnEmployee.salary = currencyTypeform(employee.type, salary);

    let payeTax = (salary > 100000) ? 1000 : 0
    returnEmployee.payeTax = currencyTypeform(employee.type, payeTax.toFixed(2));

    let netPay = salary - payeTax;
    returnEmployee.netPay = currencyTypeform(employee.type, netPay.toFixed(2));

    return returnEmployee;
}

const currencyTypeform = (currency, number) => (
    currency+' '+ number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
)

module.exports = {
    getEmployeeDetails
}
