const Fs = require("fs");
const AutoDetectDecoderStream = require("autodetect-decoder-stream");
const readline = require("readline");
const yup = require("yup");

const Employee = (
  employeeId,
  fullName,
  gender,
  dob,
  joinedDate,
  salary,
  branch
) => ({
  employeeId,
  fullName,
  gender,
  dob,
  joinedDate,
  salary,
  branch,
});

const CurrencyRate = (country, type, rate) => ({ country, type, rate });

/**
 * returns all employees
 */
const getAllEmployees = async () => {
  const filePath = __dirname.replace(
    "src/entity/employee",
    "public/static/employee.csv"
  );
  let inputStream = Fs.createReadStream(filePath).pipe(
    new AutoDetectDecoderStream({ defaultEncoding: "1255" })
  );

  const rl = readline.createInterface({
    input: inputStream,
    crlfDelay: Infinity,
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in file as a single line break.

  let employeeList = [];
  let counter = 0;
  for await (const line of rl) {
    counter++;
    if (counter == 1 || line.length == 0) {
      continue;
    }
    let row = line.split(",");

    // set validations
    let schema = yup.object().shape({
      employeeId: yup.string().matches('^EN_\\d{4}').required(),
      fullName: yup.string().required(),
      gender: yup.string(),
      dob: yup
        .string()
        .matches(
          "^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$"
        ),
      joinedDate: yup
        .string()
        .matches(
          "^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$"
        ),
      salary: yup.number().integer().required(),
      branch: yup.string().matches("[a-zA-z\\s]+").required(),
    });

    let employee = Employee(
      row[0].trim(),
      row[1].trim(),
      row[2].trim(),
      row[3].trim(),
      row[4].trim(),
      row[5].trim(),
      row[6].trim()
    );
    schema.isValid(employee).then(function (valid) {
      if (valid) {
        employee = schema.cast(employee)
        // get valid data only
        employeeList.push(employee);
      }
    });
  }

  return employeeList;
};

/**
 * returns all given currency rates
 */
const getAllCurrencyRates = async () => {
  const filePath = __dirname.replace(
    "src/entity/employee",
    "public/static/currency.csv"
  );
  let inputStream = Fs.createReadStream(filePath).pipe(
    new AutoDetectDecoderStream({ defaultEncoding: "1255" })
  );
  const rl = readline.createInterface({
    input: inputStream,
    crlfDelay: Infinity,
  });

  let currencyList = [];
  let counter = 0;
  for await (const line of rl) {
    counter++;
    if (counter == 1 || line.length == 0) {
      continue;
    }
    let row = line.split(",");

    // set validations on currency rates
    let schema = yup.object().shape({
      country: yup.string().matches("[a-zA-Zs]+").required(),
      type: yup.string().matches("[A-Z]{3}"),
      rate: yup.number().required(),
    });

    let currency = CurrencyRate(row[0].trim(), row[1].trim(), row[2].trim());

    schema.isValid(currency).then((valid) => {
      if (valid) {
        currency = schema.cast(currency)
        currencyList.push(currency);
      }
    })
  }

  return currencyList;
};

module.exports = {
  getAllEmployees,
  getAllCurrencyRates,
};
