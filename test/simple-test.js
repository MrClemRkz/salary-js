/* eslint-disable no-undef */
const app = require("../src/server");
const request = require("supertest");
const chai = require("chai");

const expect = chai.expect;

describe("#GET / ", function () {
  it("should get valid json response", function (done) {
    request(app)
      .get("/")
      .end(function (err, res) {
        expect(res.statusCode).to.equal(200);
        expect(res.body).to.be.a("object");
        done();
      });
  });

  it("should have standard response structure", (done) => {
    request(app)
      .get("/")
      .end((err, res) => {
        expect(res.body).to.have.property("success");
        expect(res.body).to.have.property("message");
        expect(res.body).to.have.property("data");
        expect(res.body).to.have.property("errors");
        done();
      });
  });
});
