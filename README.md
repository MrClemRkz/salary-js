## Basis for this development
- used the BDD approach
- CSV files are considered as the data stores here, as there are no complex logics


## How to use
1) ``` npm install ``` to get all necessary packages
2) ``` npm start ``` to start server
3) try the following rest api for test output
    - '/employees'
    - '/employees/EN_0001'

4) ``` npm test ``` to run test cases